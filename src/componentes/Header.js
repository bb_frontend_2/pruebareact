import React, { useState, useEffect} from 'react';
import './header.css';
import {Link} from "react-router-dom";
import logo from '../assets/images/logo.png';
import {Modal, Button, TextField}from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';
import axios from 'axios';
const baseUrl ="https://reqres.in/api/users/";
const useStyles = makeStyles((theme)=>({
  modal:{
    position: "absolute",
    width: 400,
    backgroundColor: '#C1D72F',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)'
  },
  iconos:{
    cursor:'pointer'
  },
  inputMaterial:{
    width: '100%'
  },
  button: {
    margin: theme.spacing(1),
  },
}));

const Header = () => {

  const styles = useStyles();
  const [data, setData]=useState([]);
  const [modalinsertar, setMoldalInsertar]=useState(false);
  const [UsuarioSeleccionado, setUsuarioSeleccionado ]=useState({
    first_name: '',
    last_name: '',
    email: '',
  })

  const peticionGet =async()=>{
    await axios.get(baseUrl)
   .then(response=>{
    console.log(response.data.data);
    setData(response.data.data);
   })

  }
  const abrirCerrarModalInsertar=()=>{
    setMoldalInsertar(!modalinsertar);
    console.log("me diste click");

  }
  const handleChange=(e)=>{
    const {name, value} = e.target;
    setUsuarioSeleccionado(prevState=>({
      ...prevState,
      [name]: value

    }))
    console.log(UsuarioSeleccionado);
  }
  const peticionPost=async()=>{
    await axios.post(baseUrl, UsuarioSeleccionado)
    .then(response=>{
      setData(data.concat(response.data))
      abrirCerrarModalInsertar()
      console.log("se a guardado nuevo usuario")
    })

  }
  const bodyInsertar = (
    <form className={styles.modal}>
      <input type="text" placeholder="First name" name="first_name" className={styles.inputMaterial} onChange= {handleChange} label="nombre"/>
      <br/>
      <input type="text" placeholder="Last name" name="last_name" className={styles.inputMaterial} onChange= {handleChange} label="apellidos"/>
      <br/>
      <input type="text" placeholder="Email" name="email" className={styles.inputMaterial} onChange= {handleChange} label="Email"/>
      <br/><br/>
      <div className="contendorCheckbox">
        <p>Enable like favorite</p>
        <input type="checkbox"></input>
      </div>
      <div align = "center">
        <Button   variant="contained" color="default" className={styles.button} variant="contained" color="default" className={styles.button} onClick={()=>peticionPost()}>Save</Button>
        <Button variant="contained" color="default" className={styles.button} onClick= {()=>abrirCerrarModalInsertar()}>Cancelar</Button>
      </div>
    </form>
   );
   useEffect(async()=>{
    await peticionGet();
  }, [])
    return (
      <>
        <div className="navar">
          <div className="logo">

            <Link to ="/">
                <img className="logo" src={logo}></img>
            </Link>
          </div>
          <div className="botones">
            <Link to ="./">
              OverView
            </Link>
            <Link to ="./contacts">
              Contacts
            </Link>

            <Link to ="./favorites">
              Favorites
            </Link>
          <button
          onClick={()=>abrirCerrarModalInsertar()}
          className="new"
          >
            + new
          </button>
          </div>
        </div>

      <Modal
      open = {modalinsertar}
      onClose= {abrirCerrarModalInsertar}
      >
      {bodyInsertar}
    </Modal>
      </>

     );

}

export default Header;