import React, {useEffect, useState} from  'react';
import './cart.css'
import {makeStyles} from '@material-ui/core/styles';
import axios from 'axios';
import {Modal, Button, TextField}from '@material-ui/core';
const useStyles = makeStyles((theme)=>({
    modal:{
      position: "absolute",
      width: 400,
      backgroundColor: '#C1D72F',
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
      top: '50%',
      left: '50%',
      transform: 'translate(-50%, -50%)'
    },
    iconos:{
      cursor:'pointer'
    },
    inputMaterial:{
      width: '100%'
    },
    button: {
      margin: theme.spacing(1),
    },
  }));

const Cart = () => {
    const baseUrl ="https://reqres.in/api/users/";
    const peticionGet =async()=>{
        await axios.get(baseUrl)
       .then(response=>{
        console.log(response.data.data);
        setData(response.data.data);
       })

    }
    const styles = useStyles();
    const [modalEditar, setModalEditar]=useState(false);
    const [modalEliminar, setModalEliminar]=useState(false);
    const [data, setData]=useState([]);
    useEffect(async()=>{
        await peticionGet();
     }, [])

    const seleccionarUsuario= (usuario, caso)=> {
        setUsuarioSeleccionado(usuario);
        (caso==='Editar')?abrirCerrarModalEditar():abrirCerrarModalEliminar();
      }
    const [UsuarioSeleccionado, setUsuarioSeleccionado ]=useState({
        first_name: '',
        last_name: '',
        email: '',
      })
      const abrirCerrarModalEditar=()=>{
        setModalEditar(!modalEditar);
      }
      const abrirCerrarModalEliminar=()=>{
        setModalEliminar(!modalEliminar);
      }
      const peticionDelete= async()=>{
        await axios.delete(baseUrl+UsuarioSeleccionado.id)
        .then(response=>{
         setData(data.filter(usuario=>usuario.id!==UsuarioSeleccionado.id))
         abrirCerrarModalEliminar();
        })
       }
      const bodyEliminar = (
        <div className={styles.modal}>
          <h3>Eliminar usuario</h3>
       <p>Esta segur@ que quieres eliminar este usuario<b>{UsuarioSeleccionado && UsuarioSeleccionado.first_name}</b>?</p>
          <br/><br/>
          <div align = "right">
            <Button variant="contained" color="default" className={styles.button}  color="secundary" onClick={()=>peticionDelete()}>Si</Button>
            <Button variant="contained" color="default" className={styles.button}  onClick= {()=>abrirCerrarModalEliminar()}>No</Button>

          </div>
        </div>
       );
       useEffect(async()=>{
        await peticionGet();
     }, [])

    return (
        <>
        <div className="contenidoApi">
        {data.map(usuario=>(
            <div className="cart" key= {usuario.id}>
            <img src={usuario.avatar}></img>
            <h5>{usuario.first_name} {usuario.last_name}</h5>
            <p>{usuario.email}</p>
            <hr className="linea"/>
            <button
                className={styles.iconos}
                onClick={()=>seleccionarUsuario(usuario, 'Eliminar')}
                className="botonEliminar"
            >
                x Remove
            </button>
        </div>

        ))}


        </div>

    <Modal
        open = {modalEliminar}
        onClose= {abrirCerrarModalEliminar}
        >
        {bodyEliminar}
    </Modal>
    </>
    );
};

export default Cart;
