import React from  'react';
import './OverView.css';
import Carts from './Cart';
const Favorites = () => {
    return (
        <div>
            <div className ="contenedor">
                <div className="contenedorTitulo">
                    <h1>Favorites</h1>
                    <hr/>
                </div>
                  <Carts/>
            </div>
        </div>

    );
};
export default Favorites;