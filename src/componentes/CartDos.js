import React, {useEffect, useState} from  'react';
import './cart.css'
import {makeStyles} from '@material-ui/core/styles';
import axios from 'axios';
import {Modal, Button, TextField}from '@material-ui/core';
import {Favorite, Delete } from '@material-ui/icons';


const useStyles = makeStyles((theme)=>({
    modal:{
      position: "absolute",
      width: 400,
      backgroundColor: '#C1D72F',
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
      top: '50%',
      left: '50%',
      transform: 'translate(-50%, -50%)'
    },
    iconos:{
      cursor:'pointer'
    },
    inputMaterial:{
      width: '100%'
    },
    button: {
      margin: theme.spacing(1),
    },
  }));

function CartDos() {
    const baseUrl ="https://reqres.in/api/users/";
    const peticionGet =async()=>{
        await axios.get(baseUrl)
       .then(response=>{
        console.log(response.data.data);
        setData(response.data.data);
       })

    }
    const styles = useStyles();
    const [modalEditar, setModalEditar]=useState(false);
    const [modalEliminar, setModalEliminar]=useState(false);
    const [data, setData]=useState([]);
    useEffect(async()=>{
        await peticionGet();
     }, [])
    const handleChange=(e)=>{
      const {name, value} = e.target;
      setUsuarioSeleccionado(prevState=>({
        ...prevState,
        [name]: value

      }))
      console.log(UsuarioSeleccionado);
    }

    const seleccionarUsuario= (usuario, caso)=> {
        setUsuarioSeleccionado(usuario);
        (caso==='Editar')?abrirCerrarModalEditar():abrirCerrarModalEliminar();
      }
    const [UsuarioSeleccionado, setUsuarioSeleccionado ]=useState({
        first_name: '',
        last_name: '',
        email: '',
      })
    const peticionPut=async()=>{
      await axios.put(baseUrl+UsuarioSeleccionado.id, UsuarioSeleccionado)
      .then(response=>{
        var dataNueva= data;
        dataNueva.map((usuario)=>{
          if(UsuarioSeleccionado.id===usuario.id){
            usuario.first_name = UsuarioSeleccionado.first_name;
            usuario.last_name = UsuarioSeleccionado.last_name;
            usuario.email = UsuarioSeleccionado.email;
          }

        })
        setData(dataNueva);
        abrirCerrarModalEditar();
      })
    }
      const abrirCerrarModalEditar=()=>{
        setModalEditar(!modalEditar);
      }
      const abrirCerrarModalEliminar=()=>{
        setModalEliminar(!modalEliminar);
      }
      const peticionDelete= async()=>{
        await axios.delete(baseUrl+UsuarioSeleccionado.id)
        .then(response=>{
         setData(data.filter(usuario=>usuario.id!==UsuarioSeleccionado.id))
         abrirCerrarModalEliminar();
        })
       }
      const bodyEliminar = (
        <div className={styles.modal}>
          <h3>Eliminar usuario</h3>
       <p>Esta segur@ que quieres eliminar este usuario<b>{UsuarioSeleccionado && UsuarioSeleccionado.first_name}</b>?</p>
          <br/><br/>
          <div align = "right">
            <Button variant="contained" color="default" className={styles.button}  color="secundary" onClick={()=>peticionDelete()}>Si</Button>
            <Button variant="contained" color="default" className={styles.button}  onClick= {()=>abrirCerrarModalEliminar()}>No</Button>

          </div>
        </div>
       );
       const bodyEditar = (
        <form className={styles.modal}>
          <h3 align= "center">Editar usuario</h3>
          <input type="text" name="first_name" className={styles.inputMaterial} onChange= {handleChange} label="nombre" value={UsuarioSeleccionado && UsuarioSeleccionado.first_name}/>
          <br/>
          <input type="text" name="last_name" className={styles.inputMaterial} onChange= {handleChange} label="apellidos" value={UsuarioSeleccionado && UsuarioSeleccionado.last_name}/>
          <br/>
          <input type="text" name="email" className={styles.inputMaterial} onChange= {handleChange} label="Email" value={UsuarioSeleccionado && UsuarioSeleccionado.email}/>
          <br/><br/>
          <div align = "center">
            <Button  variant="contained" color="default" className={styles.button} variant="contained" color="default" className={styles.button}  color="primary" onClick={()=>peticionPut()}>Editar</Button>
            <Button  variant="contained" color="default" className={styles.button} variant="contained" color="default" className={styles.button}  onClick= {()=>abrirCerrarModalEditar()}>Cancelar</Button>

          </div>
        </form>
      );
       useEffect(async()=>{
        await peticionGet();
     }, [])

    return (
        <>
        <div className="contenidoApi">
        {data.map(usuario=>(
            <div className="cart" key= {usuario.id}>
            <img src={usuario.avatar}></img>
            <h5>{usuario.first_name} {usuario.last_name}</h5>
            <p>{usuario.email}</p>
            <hr className="linea"/>
            <div className="iconos">
                <div className="contenedorIcon">
                    <Favorite onClick={()=>seleccionarUsuario(usuario, 'Editar')} className={styles.iconos} />
                </div>
                <div className="contenedorIconDelete">
                    <Delete onClick={()=>seleccionarUsuario(usuario, 'Eliminar')} className={styles.iconos} />
                </div>
            </div>
        </div>

        ))}
        </div>

    <Modal
        open = {modalEliminar}
        onClose= {abrirCerrarModalEliminar}
        >
        {bodyEliminar}
    </Modal>
    <Modal
      open = {modalEditar}
      onClose= {abrirCerrarModalEditar}
      >
        {bodyEditar}
    </Modal>
    </>
    );
}

export default CartDos;