import React from  'react';
import './OverView.css';
import Cart from './Cart';
import CartTres from './CartTres';

function OverView() {
  return (
    <div className="App">
        <div className="contenedor">
            <div className="contenedorTitulo">
                  <h1>Favorites</h1>
                  <hr/>
            </div>
            <Cart/>
            <div className="contenedorTitulo">
                  <h1> Contact List</h1>
                  <hr/>
            </div>
            <CartTres/>

        </div>
    </div>
  );
}

export default OverView;
