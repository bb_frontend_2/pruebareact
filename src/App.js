import React from  'react';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import OverView from './componentes/OverView';
import Contacts from './componentes/Contacts';
import Favorites from './componentes/Favorites'
import Header from './componentes/Header';

function App() {
  return (
    <div className="row">
    <Router>
      <Header/>
      <Switch>
        <Route path="/" exact>
          <OverView/>
        </Route>
        <Route path="/contacts" >
              <Contacts/>
        </Route>
        <Route path="/favorites" >
              <Favorites/>
        </Route>
      </Switch>
    </Router>
    <p>esta es un  pruebghfghgfa</p>
  </div>
  );
}

export default App;
